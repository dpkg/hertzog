/* canonpath.c - function to canonicalize file paths
 *
 * Adapted by Raphael Hertzog from pathcanon.c in bash-2.05a.
 *
 * Copyright (C) 1993 Free Software Foundation, Inc.
 * Copyright (C) 2007 Raphael Hertzog.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with dpkg; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* Canonicalize PATH, and return a new path.  The new path differs from PATH
 *  in that:
 *	Multiple `/'s are collapsed to a single `/'.
 *	Leading `./'s and trailing `/.'s are removed.
 *	Trailing `/'s are removed.
 *	Non-leading `../'s and trailing `..'s are handled by removing
 *	portions of the path.
 */

#include <config.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <assert.h>

#define ABSPATH(x)	((x)[0] == '/')
#define RELPATH(x)	((x)[0] != '/')
#define DIRSEP		'/'
#define ISDIRSEP(c)	((c) == '/')
#define PATHSEP(c)	(ISDIRSEP(c) || (c) == 0)

char *canonpath (char* path) {
  char stub_char;
  char *result, *p, *q, *base, *dotdot;
  int rooted;

  assert(path != NULL);

  /* The result cannot be larger than the input PATH. */
  result = path;

  if ((rooted = ABSPATH(path))) {
    stub_char = DIRSEP;
    base = result + 1;
  } else {
    stub_char = '.';
    base = result;
  }

  /*
   * invariants:
   *	  base points to the portion of the path we want to modify
   *      p points at beginning of path element we're considering.
   *      q points just past the last path element we wrote (no slash).
   *      dotdot points just past the point where .. cannot backtrack
   *	  any further (no slash).
   */
  p = q = dotdot = base;

  while (*p) {
    if (ISDIRSEP(p[0])) { /* null element */
      p++;
    } else if(p[0] == '.' && PATHSEP(p[1])) {	/* . and ./ */
      p += 1; 	/* don't count the separator in case it is nul */
    } else if (p[0] == '.' && p[1] == '.' && PATHSEP(p[2])) { /* .. and ../ */
      p += 2; /* skip `..' */
      if (q > dotdot) {	/* can backtrack */
	while (--q > dotdot && ISDIRSEP(*q) == 0)
	  ;
      } else if (rooted == 0) {
	/* /.. is / but ./../ is .. */
	if (q != base)
	  *q++ = DIRSEP;
	*q++ = '.';
	*q++ = '.';
	dotdot = q;
      }
    } else {	/* real path element */
      /* add separator if not at start of work portion of result */
      if (q != base)
	*q++ = DIRSEP;
      while (*p && (ISDIRSEP(*p) == 0))
	*q++ = *p++;
    }
  }

  /* Empty string is really ``.'' or `/', depending on what we started with. */
  if (q == result)
    *q++ = stub_char;
  *q = '\0';

  return (result);
}
